const { ClientSchemaInput } = require("../schemas/input/createClient.input");
const { ClientCreatedEvent } = require("../schemas/event/createClient.event");
const { DynamoDBClient } = require("../service/createClient.service");
const {
  sendCreateClientSNS,
} = require("../service/publishCreateClient.service");
const { ageCalc } = require("../helper/ageCalc.helper");

module.exports = async (commandPayload, commandMeta) => {
  console.log("Payload", commandPayload);
  new ClientSchemaInput(commandPayload, commandMeta);

  const age = ageCalc(commandPayload.birthDate);

  if (age < 18 || age > 65) {
    return {
      statusCode: 400,
      body: JSON.stringify({
        message: "Client must be over 18 years old and under 65 years old",
      }),
    };
  }

  await DynamoDBClient(commandPayload);
  await sendCreateClientSNS(
    new ClientCreatedEvent(commandPayload, commandMeta)
  );
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: "Client created",
    }),
  };
};

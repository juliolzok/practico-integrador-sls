const dynamodb = require("ebased/service/storage/dynamo");

const DynamoDBClient = async (payload) => {
  await dynamodb.putItem({
    TableName: process.env.CLIENT_TABLE,
    Item: payload,
  });
};

module.exports = { DynamoDBClient };

const sns = require("ebased/service/downstream/sns");

const sendCreateClientSNS = async (clientCreateEvent) => {
  const { eventPayload, eventMeta } = clientCreateEvent.get();

  await sns.publish(
    {
      TopicArn: process.env.CLIENT_SNS,
      Message: eventPayload,
    },
    eventMeta
  );
};

module.exports = { sendCreateClientSNS };
